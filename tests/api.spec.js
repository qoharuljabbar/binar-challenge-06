const request = require('supertest')
const app = require('../app.js')
const jwt = require('jsonwebtoken');
// const app = require('express')
// import app from './app.js'
const randomstring = require("randomstring")
let token;


token = jwt.sign({
    id: 6,
    username: 'edgar'
  }, 'edgar1234')

  
describe('GET /hello', () => {
    test("Return status: 200 and a hello world message", done => {
        request(app).get('/hello').then(res => {
            expect(res.statusCode).toBe(200)
    expect(res.body).toHaveProperty('status')
    expect(res.body).toHaveProperty('message')
    expect(res.body.status).toBe(true)
            expect(res.body.message).toEqual("Hello World!")
            done()
        })
    })
})

describe('Login API', () => {
    it('Success', (done) => {
      request(app)
      .post('/login')
      .send({
        username: "edgar",
        password: "edgar1234"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.header).toHaveProperty('auth-token')
          done()
        }
      })
    })
  
    it('Data Tidak Ditemukan', (done) => {
      request(app)
      .post('/login')
      .send({
        username: "edgars",
        password: "edgars"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(400)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('An error occurred')
          done()
        }
      })
    })
  })

describe('POST User', () => {
    it('Success', (done) => {
      request(app)
      .post('/users/prosesTambah')
      .set("authorization", token)
      .send({
        username: randomstring.generate({
            length: 6,
            charset: "alphabetic",
        }),
        password: randomstring.generate({
            length: 6,
            charset: "alphabetic",
        })
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(201)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Tambah Data Berhasil')
          done()
        }
      })
    })
})

describe('GET Users', () => {
    it('success', (done) => {
      request(app)
      .get('/users')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
        //   expect(Array.isArray(res.body)).toBe(true)
          done()
        }
      })
    })
})

describe('UPDATE Users', () => {
    it('Success', (done) => {
      request(app)
      .post('/users/ubah/23')
      .set('authorization', token)
      .send({
        "username": "testing",
        "password": "testing",
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Ubah Data Berhasil')
          done()
        }
      })
    })
})

describe('DELETE Users', () => {

    it('Success', (done) => {
      request(app)
      .post('/users/hapus/44')
      .set('authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Hapus Data Berhasil')
          done()
        }
      })
    })
})  

describe('POST User Biodata', () => {
    it('Success', (done) => {
      request(app)
      .post('/users/biodata/prosesTambahBiodata')
      .set("authorization", token)
      .send({
        id_usergames: randomstring.generate({
            length: 3,
            charset: "numeric",
        }),
        // id_usergames: 123,
        nama_lengkap: randomstring.generate({
            length: 10,
            charset: "alphabetic",
        }),
        ttl: "2022-05-10 07:00:00+07",
        agama: randomstring.generate({
            length: 6,
            charset: "alphabetic",
        }),
        alamat: randomstring.generate({
            length: 15,
            charset: "alphabetic",
        }),
        nomor_hp: randomstring.generate({
            length: 9,
            charset: "numeric",
        }),
        hobi: randomstring.generate({
            length: 6,
            charset: "alphabetic",
        })
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(201)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Tambah Biodata Berhasil')
          done()
        }
      })
    })
})

describe('GET Users Biodata', () => {
    it('success', (done) => {
      request(app)
      .get('/users/biodata')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
        //   expect(Array.isArray(res.body)).toBe(true)
          done()
        }
      })
    })
})

describe('UPDATE Users Biodata', () => {
    it('Success', (done) => {
      request(app)
      .post('/users/biodata/ubah/13')
      .set('authorization', token)
      .send({
        id_usergames: randomstring.generate({
            length: 3,
            charset: "numeric",
        }),
        nama_lengkap: randomstring.generate({
            length: 10,
            charset: "alphabetic",
        }),
        ttl: "2022-05-10 07:00:00+07",
        agama: randomstring.generate({
            length: 6,
            charset: "alphabetic",
        }),
        alamat: randomstring.generate({
            length: 15,
            charset: "alphabetic",
        }),
        nomor_hp: randomstring.generate({
            length: 9,
            charset: "numeric",
        }),
        hobi: randomstring.generate({
            length: 6,
            charset: "alphabetic",
        })
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Ubah Biodata Berhasil')
          done()
        }
      })
    })
})

describe('DELETE Users Biodata', () => {

    it('Success', (done) => {
      request(app)
      .post('/users/biodata/hapus/13')
      .set('authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Hapus Biodata Berhasil')
          done()
        }
      })
    })
})  

describe('POST Proses Main Game', () => {
    it('Success', (done) => {
      request(app)
      .post('/prosesMainGame')
      .set("authorization", token)
      .send({
        id_usergames: randomstring.generate({
            length: 3,
            charset: "numeric",
        }),
        // id_usergames: 123,
        nama_lengkap: randomstring.generate({
            length: 10,
            charset: "alphabetic",
        }),
        waktu_mulai: "02:57:00",
        waktu_selesai: "03:32:00",
        total_waktu_bermain: "00:35:00",
        skor: randomstring.generate({
            length: 2,
            charset: "numeric",
        })
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Tambah History Berhasil')
          done()
        }
      })
    })
})

describe('GET Riwayat', () => {
    it('success', (done) => {
      request(app)
      .get('/riwayat')
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
        //   expect(Array.isArray(res.body)).toBe(true)
          done()
        }
      })
    })
})


describe('DELETE Riwayat', () => {

    it('Success', (done) => {
      request(app)
      .post('/riwayat/hapus/12')
      .set('authorization', token)
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200)
          expect(res.body).toHaveProperty('message')
          expect(res.body.message).toBe('Hapus Riwayat Berhasil')
          done()
        }
      })
    })
})  