const express = require('express')
const { UserGame } = require('./models')
let posts = require('./db/posts.json')
const morgan = require('morgan')
const app = express()
const port = 3000
// module.exports = app.listen(3000);
const bodyParser = require("body-parser")
const cookieParser = require('cookie-parser')
const verifyToken = require('./middleware/verifyToken')
const base = require('./controllers/baseController.js')
const multer = require('multer')
const storage = require('./services/multerStorage.service')
const uploadGambar = multer(
  {
    storage: storage,
    fileFilter: (req, file, cb) => {
      if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true)
      } else {
        cb(new Error('File should be an image'), false)
      }
    },
    // limits: {
    //   fileSize: 2000
    // }
  }
)
const uploadVideo = multer(
  {
    storage: storage,
    fileFilter: (req, file, cb) => {
      if (file.mimetype === 'video/mp4' || file.mimetype === 'video/x-msvideo' || file.mimetype === 'video/quicktime') {
        cb(null, true)
      } else {
        cb(new Error('File should be an .mp4, .avi, and .mov!'), false)
      }
    },
    // limits: {
    //   fileSize: 2000
    // }
  }
)


const {
  dashboard,
  lihatDataUser, 
  tambahDataUser, 
  prosesTambahDataUser, 
  ubahDataUser, 
  prosesUbahDataUser,
  hapusDataUser, 
  lihatDataUserBiodata, 
  tambahDataUserBiodata,
  prosesTambahDataUserBiodata,
  ubahDataUserBiodata,
  prosesUbahDataUserBiodata,
  hapusDataUserBiodata,
  lihatRiwayat,
  mainGame,
  prosesMainGame,
  hapusRiwayat,
  login,
  googleAuth,
  googleUrl,
  create
} = require('./controllers/controller')


app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(cookieParser())

app.use(express.json())
app.use(express.urlencoded({extended: false}));
app.set('view engine', 'ejs')

//SWAGGER
const swaggerJSON = require('./swagger.json');
const swaggerUI = require('swagger-ui-express');
app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

app.use(morgan('combined'))

app.get('/', dashboard)
//Users
app.get('/users', lihatDataUser)
app.get('/users/tambah', tambahDataUser)
app.post('/users/prosesTambah', prosesTambahDataUser)
// app.get('/users/ubah/:id', verifyToken,ubahDataUser)
app.get('/users/ubah/:id', ubahDataUser)
app.post('/users/ubah/:id', prosesUbahDataUser)
app.post('/users/hapus/:id', hapusDataUser)
// app.delete('/users/:id', (req, res) => {
//   res.send('DELETE USER: DELETE /users/' + req.params.id);
//   });
    
//Biodata
app.get('/users/biodata', lihatDataUserBiodata)
app.get('/users/biodata/tambah', tambahDataUserBiodata)
app.post('/users/biodata/prosesTambahBiodata', prosesTambahDataUserBiodata)
app.get('/users/biodata/ubah/:id', ubahDataUserBiodata)
app.post('/users/biodata/ubah/:id', prosesUbahDataUserBiodata)
app.post('/users/biodata/hapus/:id', hapusDataUserBiodata)


    
//History Game
app.get('/riwayat', lihatRiwayat)
app.get('/mainGame/:id', mainGame)
app.post('/prosesMainGame', prosesMainGame)
app.post('/riwayat/hapus/:id', hapusRiwayat)


//Login
app.post('/login', login)

//Testing
app.get('/hello', base.index)
app.post('/sum', base.sum)

//OAuth Google
app.get('/auth/url', googleUrl)
app.get('/auth/google', googleAuth)

//Upload Multer
// app.post('/upload', upload)
app.post('/uploadGambar',
uploadGambar.single('image'),
(req, res, next) => {
  const errors = []
  if (!req.body.username) {
    errors.push('Username is required')
  }
  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
},
create
)

app.post('/uploadVideo',
uploadVideo.single('image'),
(req, res, next) => {
  const errors = []
  if (!req.body.username) {
    errors.push('Username is required')
  }
  if (errors.length > 0) {
    next({
      status: 400,
      message: errors
    })
  } else {
    next()
  }
},
create
)


module.exports = app.listen(port, () => {
  console.log(`Server nyala di port ${port}!`)
})