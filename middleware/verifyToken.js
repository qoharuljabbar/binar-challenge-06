const jwt = require ('jsonwebtoken')

function auth(req,res, next){
    const token = req.header('auth-token')
    // const cookie = req.cookie('authcookie',token,{maxAge:900000,httpOnly:true}) 
    if(!token) return res.status(401).render('login')
    
    try {
        const verifikasi = jwt.verify(token, process.env.TOKEN_RAHASIA)
        req.user = verifikasi
        next()

    } catch (error) {
        res.status(400).send('Invalid Token')
    }
}

module.exports = auth