// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCDAdKPBXG3hFPkoYc_KF0HMmnCecXYngI",
  authDomain: "binar-qoharul.firebaseapp.com",
  projectId: "binar-qoharul",
  storageBucket: "binar-qoharul.appspot.com",
  messagingSenderId: "293916264576",
  appId: "1:293916264576:web:be9f20d682afcd78587e65",
  measurementId: "G-9TV5CV4BD5"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);