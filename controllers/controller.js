const { UserGame } = require('../models')
const { UserGames_Biodata } = require('../models')
const { UserGames_History } = require('../models')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
const axios = require("axios")
const querystring = require("querystring")
dotenv.config()


function getGoogleAuthURL() {
  const rootUrl = "https://accounts.google.com/o/oauth2/v2/auth";
  const options = {
    redirect_uri: `http://localhost:3000/auth/google`,
    client_id: process.env.GOOGLE_CLIENT_ID,
    access_type: "offline",
    response_type: "code",
    prompt: "consent",
    scope: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email",
    ].join(" "),
  };

  return `${rootUrl}?${querystring.stringify(options)}`;
}

function getTokens({
  code,
  clientId,
  clientSecret,
  redirectUri,
}){
  /*
   * Uses the code to get tokens
   * that can be used to fetch the user's profile
   */
  const url = "https://oauth2.googleapis.com/token";
  const values = {
    code,
    client_id: clientId,
    client_secret: clientSecret,
    redirect_uri: redirectUri,
    grant_type: "authorization_code",
  };

  return axios
    .post(url, querystring.stringify(values), {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
    .then((res) => res.data)
    .catch((error) => {
      console.error(`Failed to fetch auth tokens`);
      throw new Error(error.message);
    });
}

function googleUrl(req, res) {
  return res.redirect(getGoogleAuthURL())
}

// Getting the user from Google with the code
async function googleAuth(req, res) {
  const code = req.query.code;

  const { id_token, access_token } = await getTokens({
    code,
    clientId: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    redirectUri: `http://localhost:3000/auth/google`,
  });

  // Fetch the user's profile with the access token and bearer
  const googleUser = await axios
    .get(
      `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${access_token}`,
      {
        headers: {
          Authorization: `Bearer ${id_token}`,
        },
      }
    )
    .then((res) => res.data)
    .catch((error) => {
      console.error(`Failed to fetch user`);
      throw new Error(error.message);
    });

  const token = jwt.sign(googleUser, process.env.TOKEN_RAHASIA);

  // res.cookie(COOKIE_NAME, token, {
  //   maxAge: 900000,
  //   httpOnly: true,
  //   secure: false,
  // });

  // res.send(googleUser);
  res.header('auth-token', token)
  res.cookie('authcookie',token,{maxAge:900000,httpOnly:true}) 
  res.status(200).json({
    message: "Login sukses"
  })
};

function dashboard (req, res){
  res.status(200).render('dashboard')
}



function lihatDataUser (req, res) {
    UserGame.findAll()
    .then(users => {
        res.status(200).render('users', { users })
    })
    .catch(err => {
        console.log(err)
    })
}

function tambahDataUser (req, res) {
    res.status(200).render('tambahUser');
  }

const prosesTambahDataUser = async (req, res, next) => {
  const user = req.body;
  await UserGame.create(user, UserGame)
    .then(() => {
      // res.status(201).redirect('/users');
      res.status(201).json({
        message: "Tambah Data Berhasil"
      })
    })
    .catch((error) => next(error.message))
}

const ubahDataUser = async (req, res) => {
    await UserGame.findByPk(req.params.id, {
    }).then(user => {
      if(user) {
        res.status(200).render('ubahUser', {user});
      }
    }) 
  }
  
  const prosesUbahDataUser = async (req, res) => {
    const user = req.body;
    await UserGame.update({ 
      username:user.username,
      password: user.password}, {
      where:{
        id:req.params.id
      }
    })
    .then(() => {
      res.status(200).json({
        message: "Ubah Data Berhasil"
      })
    })
    .catch((error) => next(error.message))

  // res.redirect(`/users/${req.params.id}`)
  // res.redirect(`/users`)
}
  

// function ubahDataUser(req, res) {
//     const { username, password } = req.body
//     const { id } = req.params
//     UserGame.update({username, password, user_id: 1}, { where: { id: id }})
//         .then(users => {
//             res.status(200).render('/users/ubah', {users})
//             // res.send('masuk view')
//             res.redirect('/users')
//         })
//         .catch(err => {
//             console.log(err)
//         })
// }


const hapusDataUser = async (req, res) => {
  await UserGame.destroy({
    where:{
      id:req.params.id
    },
  }).then(() => {
    res.status(200).json({
      message: "Hapus Data Berhasil"
    })
  }).catch(err => {
    res.status(400).json(`Data tidak dapat dihapus - ${err.message}`)
  })
}



// function hapusDataUser(req, res) {
//     const { id } = req.params
//     UserGame.destroy({ where: { id: id } })
//         .then(users => {
//             // res.status(200).json(users)
//             // res.send('masuk view')
//             res.redirect('/')
//         })
//         .catch(err => {
//             console.log(err)
//         })
// }

function lihatDataUserBiodata (req, res) {
  UserGames_Biodata.findAll()
  .then(users_biodata => {
      res.status(200).render('usersBiodata', { users_biodata })
  })
  .catch(err => {
      console.log(err)
  })
}

function tambahDataUserBiodata (req, res) {
  res.status(200).render('tambahUserBiodata');
}

const prosesTambahDataUserBiodata = async (req, res, next) => {
const users_biodata = req.body;
await UserGames_Biodata.create(users_biodata, UserGames_Biodata)
  .then(() => {
    // res.status(201).redirect('/users/biodata');
    res.status(201).json({
      message: "Tambah Biodata Berhasil"
    })
  })
  .catch((error) => next(error.message))
}

const ubahDataUserBiodata = async (req, res) => {
  await UserGames_Biodata.findByPk(req.params.id, {
  }).then(users_biodata => {
    if(users_biodata) {
      res.status(200).render('ubahUserBiodata', {users_biodata});
    }
  }) 
}

const prosesUbahDataUserBiodata = async (req, res) => {
  const users_biodata = req.body;
  await UserGames_Biodata.update({ 
    nama_lengkap:users_biodata.nama_lengkap,
    ttl: users_biodata.ttl,
    agama: users_biodata.agama,
    alamat: users_biodata.alamat,
    nomor_hp: users_biodata.nomor_hp,
    hobi: users_biodata.hobi
  }, {
    where:{
      id:req.params.id
    }
  })
  .then(() => {
    res.status(200).json({
      message: "Ubah Biodata Berhasil"
    })
  })
  .catch((error) => next(error.message))
  
// res.redirect(`/users/${req.params.id}`)
// res.redirect(`/users/biodata`)
}

const hapusDataUserBiodata = async (req, res) => {
  await UserGames_Biodata.destroy({
    where:{
      id:req.params.id
    },
  }).then(() => {
    // res.status(200).redirect('/users/biodata')
    res.status(200).json({
      message: "Hapus Biodata Berhasil"
    })
  }).catch(err => {
    res.status(400).json(`Data tidak dapat dihapus - ${err.message}`)
  })
}

function lihatRiwayat (req, res) {
  UserGames_History.findAll()
  .then(history => {
      res.status(200).render('riwayatGame', { history })
      // res.send('masuk view')
  })
  .catch(err => {
      console.log(err)
  })
}

const mainGame = async (req, res) => {
  await UserGames_Biodata.findByPk(req.params.id, {
  }).then(users_biodata => {
    if(users_biodata) {
      res.status(200).render('mainGame', {users_biodata});
    }
  }) 
}

const prosesMainGame = async (req, res) => {
  const users_biodata = req.body;
  await UserGames_History.create({ 
    id_usergames:users_biodata.id_usergames,
    username: users_biodata.nama_lengkap,
    waktu_mulai: users_biodata.waktu_mulai,
    waktu_selesai: users_biodata.waktu_selesai,
    total_waktu_bermain: users_biodata.total_waktu_bermain,
    // total_waktu_bermain: parseInt(users_biodata.waktu_selesai) - parseInt(users_biodata.waktu_mulai),
    skor: users_biodata.skor
  })
  .then(() => {
    res.status(200).json({
      message: "Tambah History Berhasil"
    })
  })
  .catch((error) => next(error.message))
  
  
  // res.redirect(`/riwayat`)
  }

  const hapusRiwayat = async (req, res) => {
    await UserGames_History.destroy({
      where:{
        id:req.params.id
      },
    }).then(() => {
      // res.status(201).redirect('/riwayat')
      res.status(200).json({
        message: "Hapus Riwayat Berhasil"
      })
    }).catch(err => {
      res.status(400).json(`Data tidak dapat dihapus - ${err.message}`)
    })
  }
  

  // auth.js
  const login = async (req, res, next) => {
    try {
      const username = req.body.username;
      const password = req.body.password;
      const getData = await UserGame.findOne({ 
        where: {
          username: username,
          password: password
        }
       })

      //token
      const token = jwt.sign({_username: getData.username}, process.env.TOKEN_RAHASIA)
      // res.header('auth-token', token).send('Berhasil login')
      
      if (!getData) {
        res.status(401).json({
          message: "Gagal Login!",
          error: "Username atau Password tidak sama!",
        })
      } else {
        res.header('auth-token', token)
        res.cookie('authcookie',token,{maxAge:900000,httpOnly:true}) 
        res.status(200).json({
          message: "Login sukses",
          getData,
        })
      } 


    } catch (error) {
      res.status(400).json({
        message: "An error occurred",
        error: error.message,
      })  
    }
  }  

  // upload
  async function create(req, res, next) {
    try {
      // upload image
      const result = await cloudinary.uploader.upload(req.file.path);
      // menghapus gambar di dalam folder public/src
      fs.unlinkSync(req.file.path)
      console.log(result)
      // masukkan ke database
      await UserGame.create({
        name: req.body.username,
        userId: req.user.id,
        image: result.secure_url
      })
      res.status(201).json({
        message: 'Succesfully create user image/video'
      })
    } catch (err) {
      next(err)
    }
  }
  



module.exports = {
  dashboard,
  lihatDataUser, 
  tambahDataUser, 
  prosesTambahDataUser, 
  ubahDataUser, 
  prosesUbahDataUser, 
  hapusDataUser, 
  lihatDataUserBiodata, 
  tambahDataUserBiodata, 
  prosesTambahDataUserBiodata,
  ubahDataUserBiodata,
  prosesUbahDataUserBiodata,
  hapusDataUserBiodata,
  lihatRiwayat,
  mainGame,
  prosesMainGame,
  hapusRiwayat,
  login,
  googleUrl,
  googleAuth,
  create
}