'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('UserGames_Histories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_usergames: {
        type: Sequelize.INTEGER
      },
      username: {
        type: Sequelize.STRING
      },
      waktu_mulai: {
        type: Sequelize.TIME
      },
      waktu_selesai: {
        type: Sequelize.TIME
      },
      total_waktu_bermain: {
        type: Sequelize.TIME
      },
      skor: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('UserGames_Histories');
  }
};