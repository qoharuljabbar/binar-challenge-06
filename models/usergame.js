'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGame.belongsTo(models.UserGames_Biodata, {
        foreignKey: 'id', 
        as: 'id_usergames',
        onDelete: 'CASCADE',
      })
      // UserGame.hasMany(models.UserGames_History, {as: 'id_usergames'})
    }
  }
  UserGame.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    created_at: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'UserGame',
  });
  return UserGame;
};