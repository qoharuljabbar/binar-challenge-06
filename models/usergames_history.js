'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGames_History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  UserGames_History.init({
    id_usergames: DataTypes.INTEGER,
    username: DataTypes.STRING,
    waktu_mulai: DataTypes.TIME,
    waktu_selesai: DataTypes.TIME,
    total_waktu_bermain: DataTypes.TIME,
    skor: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserGames_History',
  });
  return UserGames_History;
};