'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGames_Biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // UserGames_Biodata.hasMany(models.UserGames_History, {as: 'id_usergames'})
    }
  }
  UserGames_Biodata.init({
    id_usergames: DataTypes.INTEGER,
    nama_lengkap: DataTypes.TEXT,
    ttl: DataTypes.DATE,
    agama: DataTypes.STRING,
    alamat: DataTypes.TEXT,
    nomor_hp: DataTypes.INTEGER,
    hobi: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserGames_Biodata',
  });
  return UserGames_Biodata;
};